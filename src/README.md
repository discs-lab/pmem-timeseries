## visualization documents for tsbs benchmarking results

This directory contains two .ipynb files, ***benchmark-tsbs.ipynb*** and ***timescaledb_queries.ipynb***\
***benchmark-tsbs.ipynb*** contains the visualization for benchmarking insertion and query on InfluxDB and TimescaleDB\
***timescaledb_queries.ipynb*** contains the visualization for benchmarking different types of queries on Timescaledb
