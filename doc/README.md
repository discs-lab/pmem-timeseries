## Build and Install Instructions

This folder contains all the building and installation instructions needed for InfluxDB, TimescaleDB and TSBS.

* **Build TimescaleDB:** \
  TimescaleDB_from_source.txt

* **Install TimescaleDB:**\
  TimescaleDB_Installation.txt

* **Build InfluxDB:**\
  build_influxdb.pdf

* **Install InfluxDB:**\
  [download InfluxDB](https://portal.influxdata.com/downloads/)
  Or see the README.MD under [main](https://gitlab.cs.mcgill.ca/balmau/pmem-timeseries)
  
* **Install TSBS:**\
  [TSBS gitlab](https://github.com/timescale/tsbs)
  Or see the README.MD under [main](https://gitlab.cs.mcgill.ca/balmau/pmem-timeseries)
