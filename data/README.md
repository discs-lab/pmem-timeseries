## Benchmarking Results for InfluxDB and TimescaleDB

This directory contains all the benchmark results for **InfluxDB** and **TimescaleDB**. Benchmarking Tool: **TSBS**

To use these results for visualization, see directory `src`.

To reproduce these results, see  `\scripts\Insert.md` and `\scripts\Query.md`
