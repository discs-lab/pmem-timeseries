After 100 queries with 2 workers:
Interval query rate: 2.53 queries/sec   Overall query rate: 2.53 queries/sec
TimescaleDB average driver driving duration per day:
min:   775.58ms, med:   787.90ms, mean:   789.39ms, max:  862.75ms, stddev:    11.82ms, sum:  78.9sec, count: 100
all queries                                        :
min:   775.58ms, med:   787.90ms, mean:   789.39ms, max:  862.75ms, stddev:    11.82ms, sum:  78.9sec, count: 100

After 200 queries with 2 workers:
Interval query rate: 2.54 queries/sec   Overall query rate: 2.54 queries/sec
TimescaleDB average driver driving duration per day:
min:   769.34ms, med:   787.01ms, mean:   788.10ms, max:  862.75ms, stddev:     9.41ms, sum: 157.6sec, count: 200
all queries                                        :
min:   769.34ms, med:   787.01ms, mean:   788.10ms, max:  862.75ms, stddev:     9.41ms, sum: 157.6sec, count: 200

After 300 queries with 2 workers:
Interval query rate: 2.54 queries/sec   Overall query rate: 2.54 queries/sec
TimescaleDB average driver driving duration per day:
min:   765.66ms, med:   786.91ms, mean:   787.52ms, max:  862.75ms, stddev:     8.38ms, sum: 236.3sec, count: 300
all queries                                        :
min:   765.66ms, med:   786.91ms, mean:   787.52ms, max:  862.75ms, stddev:     8.38ms, sum: 236.3sec, count: 300

After 400 queries with 2 workers:
Interval query rate: 2.54 queries/sec   Overall query rate: 2.54 queries/sec
TimescaleDB average driver driving duration per day:
min:   765.66ms, med:   787.17ms, mean:   787.39ms, max:  862.75ms, stddev:     7.90ms, sum: 315.0sec, count: 400
all queries                                        :
min:   765.66ms, med:   787.17ms, mean:   787.39ms, max:  862.75ms, stddev:     7.90ms, sum: 315.0sec, count: 400

After 500 queries with 2 workers:
Interval query rate: 2.54 queries/sec   Overall query rate: 2.54 queries/sec
TimescaleDB average driver driving duration per day:
min:   765.66ms, med:   787.17ms, mean:   787.24ms, max:  862.75ms, stddev:     7.51ms, sum: 393.6sec, count: 500
all queries                                        :
min:   765.66ms, med:   787.17ms, mean:   787.24ms, max:  862.75ms, stddev:     7.51ms, sum: 393.6sec, count: 500

After 600 queries with 2 workers:
Interval query rate: 2.54 queries/sec   Overall query rate: 2.54 queries/sec
TimescaleDB average driver driving duration per day:
min:   765.66ms, med:   787.13ms, mean:   787.34ms, max:  862.75ms, stddev:     7.30ms, sum: 472.4sec, count: 600
all queries                                        :
min:   765.66ms, med:   787.13ms, mean:   787.34ms, max:  862.75ms, stddev:     7.30ms, sum: 472.4sec, count: 600

After 700 queries with 2 workers:
Interval query rate: 2.54 queries/sec   Overall query rate: 2.54 queries/sec
TimescaleDB average driver driving duration per day:
min:   765.66ms, med:   786.91ms, mean:   787.36ms, max:  862.75ms, stddev:     7.14ms, sum: 551.2sec, count: 700
all queries                                        :
min:   765.66ms, med:   786.91ms, mean:   787.36ms, max:  862.75ms, stddev:     7.14ms, sum: 551.2sec, count: 700

After 800 queries with 2 workers:
Interval query rate: 2.54 queries/sec   Overall query rate: 2.54 queries/sec
TimescaleDB average driver driving duration per day:
min:   765.66ms, med:   786.75ms, mean:   787.27ms, max:  862.75ms, stddev:     6.98ms, sum: 629.8sec, count: 800
all queries                                        :
min:   765.66ms, med:   786.75ms, mean:   787.27ms, max:  862.75ms, stddev:     6.98ms, sum: 629.8sec, count: 800

After 900 queries with 2 workers:
Interval query rate: 2.54 queries/sec   Overall query rate: 2.54 queries/sec
TimescaleDB average driver driving duration per day:
min:   765.66ms, med:   786.72ms, mean:   787.26ms, max:  862.75ms, stddev:     6.85ms, sum: 708.5sec, count: 900
all queries                                        :
min:   765.66ms, med:   786.72ms, mean:   787.26ms, max:  862.75ms, stddev:     6.85ms, sum: 708.5sec, count: 900

After 1000 queries with 2 workers:
Interval query rate: 2.55 queries/sec   Overall query rate: 2.54 queries/sec
TimescaleDB average driver driving duration per day:
min:   765.66ms, med:   786.40ms, mean:   786.94ms, max:  862.75ms, stddev:     6.86ms, sum: 786.9sec, count: 1000
all queries                                        :
min:   765.66ms, med:   786.40ms, mean:   786.94ms, max:  862.75ms, stddev:     6.86ms, sum: 786.9sec, count: 1000

Run complete after 1000 queries with 2 workers (Overall query rate 2.54 queries/sec):
TimescaleDB average driver driving duration per day:
min:   765.66ms, med:   786.40ms, mean:   786.94ms, max:  862.75ms, stddev:     6.86ms, sum: 786.9sec, count: 1000
all queries                                        :
min:   765.66ms, med:   786.40ms, mean:   786.94ms, max:  862.75ms, stddev:     6.86ms, sum: 786.9sec, count: 1000
wall clock time: 393.561169sec
