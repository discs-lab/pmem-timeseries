# Insertion to InfluxDB and TimescaleDB with TSBS

## 
All the following instruction is executed under the tsbs directory.

* Step1. Prepare dataset

```sh 
tsbs_generate_data --use-case="iot" --seed=123 --scale=4000   
--timestamp-start="2021-01-01T00:00:00Z"  
--timestamp-end="2021-01-04T00:00:00Z"   
--log-interval="60s" --format="timescaledb" | gzip > /tmp/timescaledb-data.gz
```

Here, you could change the **timestamp-start** and **timestamp-end="2021-01-04T00:00:00Z"** with the time interval you like.

Change **log-interval** to be the interval for data generation.

Change **format** to either **influx** or **timescaledb**

The generated dataset is store under /tmp as timescaledb-data.gz or influx-data.gz
Avoid changing this data name. Otherwise, you'll have to rename the functions in the tsbs scripts.

* Step2. Open databases server
Run ```influxd``` to start influx server
Run ```psql``` to start timescaledb server

* Step3. Insert dataset
For TimescaleDB
```bash
NUM_WORKERS=2 BATCH_SIZE=10000 BULK_DATA_DIR=/tmp scripts/load/load_timescaledb.sh >> [your_results].txt
```

For InfluxDB
```
NUM_WORKERS=2 BATCH_SIZE=10000 BULK_DATA_DIR=/tmp scripts/load/load_influx.sh >> [your_results].txt
```
Changing [your_results].txt to the name of file you'd like to store the results to. Otherwise, the performance results are printed  as stdout.
Remember to run ```chmod 777 [your_results].txt``` to give write permission.

