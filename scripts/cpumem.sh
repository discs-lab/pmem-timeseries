#!/bin/bash
echo "%CPU,%MEM" > cpu_test.csv
pid=$1
filename=$2
flag=1
echo $pid
while [ "$flag" -eq 1 ]
do
        `top -p ${pid} -b -n1 >> $filename`
        if pgrep -x "$pid" >/dev/null
        then
                flag=0
                break
        fi
        sleep 10
done

