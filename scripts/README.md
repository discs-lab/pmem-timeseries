## Running Scripts for Using and Monitoring TSBS 

This folder contains all the running scripts for using TSBS and monitoring TSBS.

### Using TSBS
* Data Generation and Insertion:\
  `Insert.md`
* Query Generation and Execution:\
  `Query.md`
  
### Monitoring CPU/MEM 
* Shell Scripts:\
  `cpumem.sh`\
  `cpumem_grep.sh`
* Instruction for using the above scripts:\
  `cpu.md`
