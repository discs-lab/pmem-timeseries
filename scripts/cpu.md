## To use **cpumem.sh** under ```/scripts```:

First create the log txt files, and run ```chmod 777 [filename]``` to give permision\
then run:
```sh
./cputest.sh [pid] [filename]
```
pid is the process we want to monitor and filename is the name for the log file.

## To use **cpumem_grep.sh** under ```/scripts```:

First create the log txt files, and run ```chmod 777 [filename]``` to give permision\
then run:

```sh
./cpumem_grep.sh [filename]

```
cpumem_grep.sh is by default monitoring influxdb. \
To change it, simply replace the keyword **influx** in line 6 into **timescaledb**
