# Query on InfluxDB and TimescaleDB with TSBS

## 
All the following instruction is executed under the tsbs directory.

* Step1. Generate Random Queries
```sh 
tsbs_generate_queries --use-case="iot" --seed=123 --scale=4000 --timestamp-start="2021-01-01T00:00:00Z"  
--timestamp-end="2021-01-01T00:40:00Z" --queries=1000 --query-type="breakdown-frequency" 
--format="timescaledb" | gzip > /tmp/[user_defined].gz
```

Here, you could change the **timestamp-start** and **timestamp-end="2021-01-04T00:00:00Z"** with the time interval you like. Make sure it is consistentwith the data timestamps you have in the database.

Change **queries** to the number of queries you want. TSBS print performance results for every 100 queries.

Change **format** to either **influx** or **timescaledb**

Change **query-type** to any of the following Query Type:

| Query Type  | Query Description |
| ------------- | ------------- |
| last-loc |Fetch real-time (i.e. last) location of each truck|
|low-fuel|Fetch all trucks with low fuel (less than 10%)|
|high-load|Fetch trucks with high current load (over 90% load capacity)|
|stationary-trucks|Fetch all trucks that are stationary (low avg velocity in last 10 mins)|
|long-driving-sessions|Get trucks which haven't rested for at least 20 mins in the last 4 hours|
|long-daily-sessions|Get trucks which drove more than 10 hours in the last 24 hours|
|avg-vs-projected-fuel-consumption|Calculate average vs. projected fuel consumption per fleet|
|avg-daily-driving-duration|Calculate average daily driving duration per driver|
|avg-daily-driving-session|Calculate average daily driving session per driver|
|avg-load|Calculate average load per truck model per fleet|
|daily-activity|Get the number of hours truck has been active (vs. out-of-commission) per day per fleet|
|breakdown-frequency  | Calculate breakdown frequency by truck model|


The generated dataset is store under /tmp as [user_defined].gz. Replace it with the name you like.

* Step2. Query Execution\
For TimescaleDB:
```sh
cat /tmp/[user_defined].gz | gunzip | tsbs_run_queries_timescaledb 
--workers=2 --postgres="host=localhost user=postgres sslmode=disable" --pass="password"
```
For InfluxDB:
```sh
cat /tmp/[user_defined].gz | gunzip | tsbs_run_queries_influx --workers=2
```

The query performance is printed to stdout by default. You could optionally redirect it to files.

