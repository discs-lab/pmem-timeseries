# Persistent Memory in Timeseries
<div id="top"></div>  
Authors: Ruoyu Wang and Jiaxuan Chen

COMP-400 Research Project

## About The Project
The variable of time has become a crucial factor in the analysis and usage of data as it helps organizations for better understanding of the trends and patterns over time, eventually used in future prediction. Time-Series data and Time-Series Database (TSDB) are thus developed for optimized usage of the time feature. Solving the database bottleneck and improving system efficiency become crucial to make improvements.<br/> 

Motivated by solving the bottleneck of the Time-Series Database, we conducted this research project to study the functionalities and properties of Time Series databases and to further compare their performance in inserting a and various kinds of querying. <br/>

In this project, we explored the installation and build work for InfluxDB and TimescaleDB. We deployed the benchmark framework, TSBS, for automatic data generation and database performance benchmarking.

This project serves as a baseline and preparation work for our goal in eventually deploying new storage technologies such as Persistent Memory (PMEM) in the TSDB to verify the impact of emerging storage technologies on reimagining the way data-intensive storage systems work.
<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

* [InfluxDB](https://www.influxdata.com)
* [TimescaleDB](https://www.timescale.com)
* [PostgreSQL](https://www.postgresql.org)
* [TSBS](https://github.com/timescale/tsbs)
* [Go](https://go.dev)
<p align="right">(<a href="#top">back to top</a>)</p>

## Getting Started (TSDB)

### Install InfluxDB
For simple use of InfluxDB with no need to modify the source code, we installed influxdb version 1.8.10 which is supported by the benchmark tool. Note that the latest version of InfluxDB is not supported by TSBS. Currently we stick to version 1.8.10.

* install influxdb 1.8.10 on ubuntu
  ```sh
  wget https://dl.influxdata.com/influxdb/releases/influxdb_1.8.10_amd64.deb
  sudo dpkg -i influxdb_1.8.10_amd64.deb
  ```
Link to download influxdb: https://docs.influxdata.com/influxdb/v2.0/install/
<p align="right">(<a href="#top">back to top</a>)</p>

### Build InfluxDB Server from Source
For our future work, to leverage Persistent Memory on data-intensive applications like InfluxDB, we have to modify the source code and embed program chunks of using PMEM. It is neccessary to have all dependencies for building the database and to successfully build it from source on DISCS server.

#### Installation of Dependencies
Prerequisites:
Please check if all dependencies are installed.
go1.17, bazaar, rust, git, make, clang, pkg-config, protobuf.

Build InfluxDB from source on Unbuntu 20.04.3.

* install curl
  ```sh
  sudo apt install curl
  ```
  curl version 7.68.0 works with no error while curl with higher versions may cause conflicts with rustc. apt-install should by default install version 7.68.0.

* install git
  ```sh
  sudo apt-get install git
  ```
* install gvm
  ```sh
  bash < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/ binscripts/gvm-installer)
  ```
  Link to gvm github: https://github.com/moovweb/gvm
  
* Run source command to reload the changed bash setting for the current running bash. \
  The command to be executed should have been prompted after gvm is installed successfully. \
  For me, the command is: source /home/ joy/.gvm/scripts/gvm
* install binutils
  ```sh
  sudo apt-get install binutils
  ```
* install bison
  ```sh
  sudo apt-get install bison
  ```
* install gcc
  ```sh
  sudo apt-get install gcc
  ```
* install go1.17
    1. Install go 1.4 first. Installing go version higher than 1.4 requires installing go 1.4.
    ```sh
    gvm install go1.4 -B
    ```
    2. Switch to go1.4
    ```sh
    gvm use go1.4 -B
    ```
    3. Set goroot
    ```sh
    EXPORT GOROOT_BOOTSTRAP=$GOROOT
    ```
    4. Install go1.17
    ```sh
    gvm install go1.17 
    ```
    5. Switch to go1.17
    ```sh
    gvm use go1.17 —default
    GO111MODULE=on
    ```
* install bazaar
  ```sh
  sudo easy_install-2.7 bzr
  ```
  2.7 refers to the python version. Make sure you have python2.7 and the python-dev package which contains the Python.h. \
  If not, run ```sudo apt install python2```and ```sudo apt-get install python-dev``` to install them.
* install rustup
  ```sh
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
  source $HOME/.cargo/env
  rustup default nightly
  ```
* install all the remaining dependencies
  ```bash
  sudo apt install make clang pkg-config protobuf-compiler libprotobuf-dev
  ```
Congratulations! You're now ready to build influxdb from source.

<p align="right">(<a href="#top">back to top</a>)</p>

#### Compiling InfluxDB Server
* fetch the source code
  ```sh
  git clone https://github.com/influxdata/influxdb
  ```
* build
  ```sh
  cd influxdb
  make
  ```
After the build finishes with no error, you'll have the executable located at ```bin/$(GOOS)/influxd```

<p align="right">(<a href="#top">back to top</a>)</p>

### Build InfluxDB CLI from Source
After successful building the influxdb's server as the database backend, now we could build the CLI for managing resources.

* fetch source code
  ```sh
  git clone https://github.com/influxdata/influx-cli
  ```
* build
  ```make``` or ```make influx```
After the build finishes with no error, you'll have the executable located at ```bin/$(GOOS)/influx```


### Install TimescaleDB
Please see instructions here: https://gitlab.cs.mcgill.ca/balmau/pmem-timeseries/-/blob/main/TimescaleDB_from_source.txt


### Build TimescaleDB from source
Please see instructions here: https://gitlab.cs.mcgill.ca/balmau/pmem-timeseries/-/blob/main/TimescaleDB_Installation.txt
<p align="right">(<a href="#top">back to top</a>)</p>

## Getting Started (TSBS)

After building/installing the databases, we now move on to install our benchmark framework -- TSBS.
### About TSBS
TSBS is used to benchmark bulk load performance and
query execution performance. (It currently does not measure
concurrent insert and query performance, which is a future priority.)
To accomplish this in a fair way, the data to be inserted and the
queries to run are pre-generated and native Go clients are used
wherever possible to connect to each database

### Installation
The easiest way to get and install the Go programs is to use
`go get` and then `make all` to install all binaries:
* Fetch TSBS and its dependencies
  ```sh
  go get github.com/timescale/tsbs
  cd $GOPATH/src/github.com/timescale/tsbs
  make
  ```
<p align="right">(<a href="#top">back to top</a>)</p>

## Use TSBS to Benchmark Performance on InfluxDB and TimescaleDB

The general working pipeline of using TSBS is:
1. Data Generation
2. Data Insertion
3. Query Generation
4. Query Execution

### directory `scripts`
We have summarised all usage cases under the directory [`/scripts`](https://gitlab.cs.mcgill.ca/balmau/pmem-timeseries/-/tree/main/scripts) including:

  Insert.md: includes all the commands needed to generate dataset and insert dataset into InfluxDB and TimescaleDB.\
  Query.md: includes all the commands needed to generate random queries and execute the quries on InfluxDB and TimescaleDB.

To use, change directory to tsbs, and execute corresponding commands directly. If prompted as "command not found", run 

  `EXPORT PATH=$PATH:path/to/tsbs`

We also have separate cpu/mem monitoring shell scripts with separate README.md. under the directory [`/scripts`](https://gitlab.cs.mcgill.ca/balmau/pmem-timeseries/-/tree/main/scripts)


### directory `data`
Sample benchmark results are under the directory [`/data`](https://gitlab.cs.mcgill.ca/balmau/pmem-timeseries/-/tree/main/data) :

`/data` contains:
Directory `InfluxDB` contains the results for insertion and query performance on different size of data. CPU/MEM record is also included.

Directory `TimescaleDB` contains the results for insertion and query performance on different size of data. It has a subdirectory `Queries` which contins the results for running different types of queries on TimescaleDB.

### directory `doc`:
Instructions for install/build the databases and benchmarking tools are under [`/doc`](https://gitlab.cs.mcgill.ca/balmau/pmem-timeseries/-/tree/main/doc)

### directory `src`:
.ipynb files for visualizing the data under [`/src`](https://gitlab.cs.mcgill.ca/balmau/pmem-timeseries/-/tree/main/src)


## Visualization
We make data visualization on Colab by using Python matplot library. The .ipynb sciprts is under the directory ```/src```. We used the results in the data directory.
<p align="right">(<a href="#top">back to top</a>)</p>
